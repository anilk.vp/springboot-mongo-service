package org.example.user;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserRepository userRepository;

    @GetMapping
    public List<User> getAll(){
        return userRepository.findAll();
    }

    @GetMapping("/{id}")
    public User get(@PathVariable("id") String id){
        Optional<User> optionalUser = userRepository.findById(id);
        if(optionalUser.isPresent()){
            return optionalUser.get();
        }
        return null;
    }

    @PostMapping
    public String create(@Valid @RequestBody  User user){
        return userRepository.save(user).getId();
    }

    @PutMapping
    public User update(@Valid @RequestBody  User user) {
        return userRepository.save(user);
    }

    @DeleteMapping("/{id}")
    public boolean delete(@PathVariable("id") String id){
        userRepository.deleteById(id);
        return true;
    }

}
